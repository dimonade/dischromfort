#[derive(Debug)]
struct RoundFormat {
    inner_hole_radius: u64,
    inner_write_area_radius: u64,
    outer_write_area_radius: u64,
    outer_radius: u64,
}

const CompactDisc: RoundFormat = RoundFormat {
    inner_hole_radius: 33,
    inner_write_area_radius: 41,
    outer_write_area_radius: 117,
    outer_radius: 125,
};

const Vinyl: RoundFormat = RoundFormat {
    inner_hole_radius: 0,
    inner_write_area_radius: 0,
    outer_write_area_radius: 0,
    outer_radius: 0,
};

const MiniCompactDisc: RoundFormat = RoundFormat {
    inner_hole_radius: 0,
    inner_write_area_radius: 0,
    outer_write_area_radius: 0,
    outer_radius: 0,
};


fn calculate_radii(args: &clap::ArgMatches) -> RoundFormat {
    let disc_format = args.get_one::<String>("disc-format").expect("Can't happen here.");
    let output_size = args.get_one::<u64>("output-size").expect("Can't happen here.");
    if disc_format == "cd" {
        let ratio: u64 = (output_size / (2 * CompactDisc.outer_radius)) as u64;
        let cd_pixel_limits: RoundFormat = RoundFormat {
            inner_hole_radius: ratio * CompactDisc.inner_hole_radius,
            inner_write_area_radius: ratio * CompactDisc.inner_write_area_radius,
            outer_write_area_radius: ratio * CompactDisc.outer_write_area_radius,
            outer_radius: ratio * CompactDisc.outer_radius,
        };
        cd_pixel_limits
    } else {
        CompactDisc
    }

}

pub fn encode(encode_args: &clap::ArgMatches) {
    println!("Encoding data with the following parameters:");
    println!("Source: {:?}", encode_args.get_one::<String>("source"));
    println!(
        "Disc format: {:?}",
        encode_args.get_one::<String>("disc-format")
    );
    println!(
        "Data type: {:?}",
        encode_args.get_one::<String>("data-type")
    );
    println!(
        "Output size: {:?}",
        encode_args.get_one::<u64>("output-size")
    );
    println!(
        "Press format: {:?}",
        encode_args.get_one::<String>("press-format")
    );
    let output_name = match encode_args.get_one("output-name") {
        Some(name) => name,
        None => encode_args
            .get_one::<String>("source")
            .expect("Must have a source."),
    };
    println!("Output name: {:?}", output_name);
    

    let radii = calculate_radii(encode_args);
    println!("{:?}", radii);
}
/*
#[cfg(test)]
mod tests {
    use crate::encode;

    #[test]
    fn test_encode_cd_from_text_file() {
        let result = encode();
        assert_eq!();
    }
}
*/
