// use std::fs;

use clap::{App, Arg, Command};
use dischromfort::encode;



fn main() {
    // TODO: Limit the output-size to be at least the minimum outer radius.
    // Otherwise we will have to deal with floats.
    let args = App::new("Dischromfort")
        .author("Dima S. and ...")
        .version("0.1.0")
        .about("Dischromfort hides your data in plain sight!")
        .subcommand_required(true)
        .arg(
            Arg::with_name("verbose")
                .short('v')
                .long("verbose")
                .help("Verbosity level"),
        )
        .subcommand(
            Command::new("encode")
                .about("Encodes your data.")
                .arg_required_else_help(true)
                .arg(Arg::with_name("source").required(true))
                .arg(
                    Arg::with_name("disc-format")
                        .long("disc-format")
                        .required(false)
                        .value_parser(["cd", "vinyl", "mini-cd"])
                        .default_value("cd"),
                )
                .arg(
                    Arg::with_name("data-type")
                        .long("data-type")
                        .required(false)
                        .value_parser(["file", "stream", "socket"])
                        .default_value("file"),
                )
                .arg(
                    Arg::with_name("output-size")
                        .long("output-size")
                        .required(false)
                        .value_parser(clap::value_parser!(u64).range(128..2048))
                        .default_value("1024"),
                )
                .arg(
                    Arg::with_name("press-format")
                        .long("press-format")
                        .required(false)
                        .value_parser(["radial", "linear"])
                        .default_value("radial"),
                )
                .arg(
                    Arg::with_name("output-name")
                        .long("output-name")
                        .required(false),
                ),
        )
        .subcommand(
            Command::new("decode")
                .about("Decodes your data.")
                .arg_required_else_help(true),
        )
        .get_matches();

    match args.subcommand() {
        Some(("encode", sub_matches)) => {
            encode(sub_matches);
        }
        Some(("decode", sub_matches)) => println!("Got `decode` with {:?}", sub_matches),
        _ => unreachable!("Exhausted."),
    }
}
